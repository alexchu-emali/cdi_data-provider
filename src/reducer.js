import Moment from 'react-moment';

export function formatDateTime(dateTime) {
    return (
        // <Moment date={date} format="DD MMM yyyy" />
        <Moment date={dateTime} format="DD MMM yyyy, hh:mm" />
    )
}