
export const queryCdiConsent = async (id) => {
    // try {
    //     let response = await axios({
    //         method: 'post',
    //         url: BACKEND_SERVER + '/api/consent/query',
    //         headers: {
    //             'Content-Type': 'application/json;charset=utf-8'
    //         },
    //         data: {
    //             query: {
    //                 issuer: "id",
    //                 status: ["0", "1", "2", "3"]
    //             },
    //             limit: 1000,
    //             page: 1
    //         }
    //     });
    //     let body = response.data
    //     if (body.code === 200) {
    //         console.log(body);
    //         return body.data;
    //     }
    // } catch (e) {
    //     console.error("query consent by bank id error");
    //     console.error(e);
    // }
    // return null;

    return {
        data:[
                {
                    updateTime: "2021-01-01T00:00:00.000Z",
                    consentdid: "001",
                    dataProvider:"data provider 001",
                    status:0,
                    data:'{ "consentInformation":{ "dataConsumer":"standard Chartered", "dataProvider":"Abstergi Ltd", "dataOwner":"Dennard & McShane", "did":"SD9212901", "purpose":"lorem lpsum", "docRefNo":"2345", "accessMode":"loerm lpsum", "status":0 }, "dataScope":{ "scopeType":"loerm lpsum", "docType":"loerm lpsum", "udr":"loerm lpsum", "accessMode":"loerm lpsum", "startDate":"loerm lpsum", "endDate":"loerm lpsum", "queryFrequencyType":"loerm lpsum" } }'
                
                },
                {
                    updateTime: "2021-01-20T16:00:00.000Z",
                    consentdid: "002",
                    dataProvider:"data provider 002",
                    status:1,
                    data:'{ "consentInformation":{ "dataConsumer":"standard Chartered", "dataProvider":"Abstergi Ltd", "dataOwner":"Dennard & McShane", "did":"SD9212901", "purpose":"lorem lpsum", "docRefNo":"2345", "accessMode":"loerm lpsum", "status":1 }, "dataScope":{ "scopeType":"loerm lpsum", "docType":"loerm lpsum", "udr":"loerm lpsum", "accessMode":"loerm lpsum", "startDate":"loerm lpsum", "endDate":"loerm lpsum", "queryFrequencyType":"loerm lpsum" } }'
                
                },
                {
                    updateTime: "2021-01-20T16:00:00.000Z",
                    consentdid: "002",
                    dataProvider:"data provider 002",
                    status:2,
                    data:'{ "consentInformation":{ "dataConsumer":"standard Chartered", "dataProvider":"Abstergi Ltd", "dataOwner":"Dennard & McShane", "did":"SD9212901", "purpose":"lorem lpsum", "docRefNo":"2345", "accessMode":"loerm lpsum", "status":2 }, "dataScope":{ "scopeType":"loerm lpsum", "docType":"loerm lpsum", "udr":"loerm lpsum", "accessMode":"loerm lpsum", "startDate":"loerm lpsum", "endDate":"loerm lpsum", "queryFrequencyType":"loerm lpsum" } }'
                
                }
        ]
    }
}


export const queryDataRequest = async (id) => {
    // try {
    //     let response = await axios({
    //         method: 'post',
    //         url: BACKEND_SERVER + '/api/consent/query',
    //         headers: {
    //             'Content-Type': 'application/json;charset=utf-8'
    //         },
    //         data: {
    //             query: {
    //                 issuer: "id",
    //                 status: ["0", "1", "2", "3"]
    //             },
    //             limit: 1000,
    //             page: 1
    //         }
    //     });
    //     let body = response.data
    //     if (body.code === 200) {
    //         console.log(body);
    //         return body.data;
    //     }
    // } catch (e) {
    //     console.error("query consent by bank id error");
    //     console.error(e);
    // }
    // return null;

    return {
        data:[
                {
                    updateTime: "2021-01-01T00:00:00.000Z",
                    consentdid: "001",
                    dataProvider:"data provider 001",
                    status:0
                
                },
                {
                    updateTime: "2021-01-20T16:00:00.000Z",
                    consentdid: "002",
                    dataProvider:"data provider 002",
                    status:1
                },

        ]
    }
}