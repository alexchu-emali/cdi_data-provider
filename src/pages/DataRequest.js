import React, { useMemo, useEffect, useState } from 'react';
import './DataRequest.css';
import Sidebar from '../components/Sidebar';

import { useTable, useSortBy, useExpanded, usePagination } from 'react-table';
import { AiOutlineRight, AiOutlineLeft } from "react-icons/ai";
import { IoClose } from "react-icons/io5";
import sort from '../img/icon/sort.svg';
import sortDown from '../img/icon/sortDown.svg';
import sortUp from '../img/icon/sortUp.svg';

import { queryDataRequest } from '../api/backend';
import { formatDateTime } from '../reducer';
import { Divider } from '@material-ui/core';

function DataRequest() {
    const [dataRequest, setDataRequest] = useState([]);

    useEffect(() => {
        async function fetchData() {
            let Id = '123';
            let result = await queryDataRequest(Id);
            // console.log(result);
            if (result) {
                setDataRequest(result.data);
            }
        }
        fetchData();
    }, [])

    let data;
    data = useMemo(() => dataRequest)

    const columns = useMemo(
        () => [
            {
                Header: 'LAST UPDATE',
                accessor: 'updateTime', // accessor is the "key" in the data
                Cell: ({ cell: { value } }) =>
                    formatDateTime(value)
            },
            {
                Header: 'CONSENT DID',
                accessor: 'consentdid',
            },
            {
                Header: 'DATA PROVIDER',
                accessor: 'dataProvider',
            },
            {
                Header: 'STATUS',
                accessor: 'status',
                Cell: ({ cell: { value } }) =>
                    statusHandler(value)
            },
            {
                Header: 'ACTION',
                id: "action",
                accessor: 'status',
                Cell: ({ cell: { row } }) => {
                    let status = JSON.parse(row.values.status)
                    let action;
                    (status == 0)
                        ? action = <span onClick={() => revoke(row.values.consentdid)} style={{ color: "#036aa4",cursor:'pointer' }}>Revoke</span>
                        : action = <span {...row.getToggleRowExpandedProps()}>
                            {row.isExpanded ? <IoClose /> : <span className="view-btn">Upload Data</span>}
                        </span>
                    return (
                        action
                    )

                },
            },
        ],
        []
    )

    function statusHandler(status) {
        switch (status) {
            case 0:
                return (
                    <div className="status purple">
                        DATA REQUEST PENDING
                    </div>
                )
            case 1:
                return (
                    <div className="status blue">
                        DATA OBTAINED
                    </div>
                )
        }
    }

    function Table({ columns, data, renderRowSubComponent }) {
        // Use the state and functions returned from useTable to build your UI
        const {
            getTableProps,
            getTableBodyProps,
            headerGroups,
            prepareRow,
            page,
            rows,
            canPreviousPage,
            canNextPage,
            pageOptions,
            pageCount,
            gotoPage,
            nextPage,
            previousPage,
            setPageSize,
            visibleColumns,
            state: { pageIndex, pageSize, expanded },
        } = useTable(
            {
                columns,
                data,
                initialState: {
                    pageIndex: 0, sortBy: [
                        {
                            id: 'updateTime',
                            desc: true
                        }
                    ]
                },
            },
            useSortBy,
            useExpanded,
            usePagination
        )

        // render table UI
        return (
            <>
                <table {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        <div className="tableHeader">
                                            <div className="pr-1">{column.render('Header')}</div>
                                            <span>
                                                {column.isSorted
                                                    ? column.isSortedDesc
                                                        ? <img src={sortDown} width="20" height="20" />
                                                        : <img src={sortUp} width="20" height="20" />
                                                    : <img src={sort} width="20" height="20" />}
                                            </span>
                                        </div>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    {/* <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                            )
                        })}
                    </tbody> */}
                    <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                                <React.Fragment {...row.getRowProps()}>
                                    <tr>
                                        {row.cells.map(cell => {
                                            return (
                                                <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                            )
                                        })}
                                    </tr>
                                    {row.isExpanded ? (
                                        <tr>
                                            <td colSpan={visibleColumns.length} style={{ padding: '0px' }}>
                                                {renderRowSubComponent({ row })}
                                            </td>
                                        </tr>
                                    ) : null}
                                </React.Fragment>
                            )
                        })}
                    </tbody>
                </table>
                {/* 
                Pagination can be built however you'd like. 
                This is just a very basic UI implementation:
                */}
                <div className="pagination">

                    <div>Showing 1 - {(rows.length <= 10) ? rows.length : pageSize} of {rows.length} results</div>
                    <div>
                        <span className="pr-1">Results per page:</span>
                        <select
                            value={pageSize}
                            onChange={e => {
                                setPageSize(Number(e.target.value))
                            }}
                            className="pageSelect"
                        >
                            {[10, 20, 30, 40, 50].map(pageSize => (
                                <option key={pageSize} value={pageSize}>{pageSize}
                                </option>
                            ))}
                        </select>
                    </div>



                    <div className="pages-controller">

                        <button className="pr-1" onClick={() => previousPage()} disabled={!canPreviousPage}>
                            {<AiOutlineLeft />}<span className="pl-1"> PREVIOUS</span>
                        </button>{' '}
                        <span className="pageInput-wrapper">
                            <input
                                type="number"
                                defaultValue={pageIndex + 1}
                                onChange={e => {
                                    const page = e.target.value ? Number(e.target.value) - 1 : 0
                                    gotoPage(page)
                                }}
                                className="pageInput"
                            />
                            of <span style={{ color: '#333' }}>{pageOptions.length}</span>
                        </span>
                        <button className="pl-1" onClick={() => nextPage()} disabled={!canNextPage}>
                            <span className="pr-1">NEXT</span> {<AiOutlineRight />}
                        </button>{' '}

                    </div>
                </div>
            </>
        )
    }


    // table detail wrapper
    const renderRowSubComponent = React.useCallback(
        ({ row }) => {
            // let data = JSON.parse(row.);
            let did =row.values.consentdid;
            return (
                <div className="detail-wrapepr" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', background: '#f9fcfe', padding: '10px 20px', borderLeft: '5px solid #c56612' }}>
                    <div className="flex-row" style={{ display: 'flex', flexDirection: 'row' }}>

                        <div className="content" style={{ flex: '1' }}>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', width: '90%', }}>
                                <div className="info-group" style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                                    <label for="apiEndPoint" style={{ fontSize: '12px', color: '#a3a3a3', marginBottom: '5px' }}>API Endpoint URL</label>
                                    <input type="text" id="apiEndPoint" name="apiEndPoint" style={{ height: '60px', border: '1px solid #e0e0e0', borderRadius: '5px' }} />
                                </div>

                            </div>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: '10px', width: '90%' }}>
                                <div className="info-group" style={{ paddingRight: '50px', width: '100%'}}>
                                    <label for="username" style={{ fontSize: '12px', color: '#a3a3a3', marginBottom: '5px' }}>Username</label><br></br>
                                    <input type="text" id="username" name="username" style={{ height: '30px', border: '1px solid #e0e0e0', borderRadius: '5px', width: '100%' }} />
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '100%' }}>
                                    <label for="password" style={{ fontSize: '12px', color: '#a3a3a3', marginBottom: '5px' }}>Password</label><br></br>
                                    <input type="text" id="password" name="password" style={{ height: '30px', border: '1px solid #e0e0e0', borderRadius: '5px', width: '100%' }} />

                                </div>
                                <div className="info-group" style={{ width: '100%', padding: '0px' }}>
                                    <label for="token" style={{ fontSize: '12px', color: '#a3a3a3', marginBottom: '5px' }}>Token</label><br></br>
                                    <input type="text" id="token" name="token" style={{ height: '30px', border: '1px solid #e0e0e0', borderRadius: '5px', width: '100%' }} />

                                </div>

                            </div>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end',width:'90%', marginTop: '10px' }}>
                                <div className="info-group" style={{ width: '150px',height:'35px' }} onClick={() => upload(did)}>
                                    <div style={{ color: '#fff', fontSize: '14px',background:'#036aa4',textAlign:'center',padding:'8px',borderRadius:'5px' }}>
                                        Upload
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    {/* <p>{data.consentInformation.dataConsumer}</p> */}

                </div>
            )
        },
        []
    )

    function revoke(did){
        console.log('revoke '+ did);
    }

    function upload(did){
        console.log('upload '+ did)
    }

    return (
        <>
            <div className="container">
                <div className="container-inner">
                    <Sidebar />
                    <div className="main-wrapper">
                        <div className="page-head">
                            <p className="page-title">
                                Data Requests
                        </p>
                            <div className="hr"></div>
                        </div>

                        <div className="filter-wrapper">
                            <div className="filter-form">
                                <input className="filter-search-input" placeholder="Search by DID" />
                                <div className="filter-select">
                                    <select name="dateRange" id="dateRange" style={{ width: '175px' }}>
                                        <option value="dateRange">Date Range</option>
                                    </select>
                                    <select name="dataOwner" id="dataOwner" style={{ width: '255px' }}>
                                        <option value="dataOwner">Data Owner</option>
                                    </select>
                                    <select name="dataProvider" id="dataProvider" style={{ width: '255px' }}>
                                        <option value="dataProvider">Data Provider</option>
                                    </select>
                                </div>

                            </div>

                            <div className="hr"></div>

                        </div>

                        <div className="dataRequest-table-wrapper">
                            <div className="dataRequestList-table-wrapper">
                                {dataRequest != '' ? <Table columns={columns} data={data} renderRowSubComponent={renderRowSubComponent} /> : 'No Data Requests yet'}
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </>
    )
}

export default DataRequest
